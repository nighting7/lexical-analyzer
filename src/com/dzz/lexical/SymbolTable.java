package com.dzz.lexical;

/**
 * 符号表，此类为关键词，标识符，运算符，边界符，常数等进行种类编码
 */
public class SymbolTable {
    //关键字
    public static final int WHILE = 1;
    public static final int FOR = 2;
    public static final int CONTINUE = 3;
    public static final int BREAK = 4;
    public static final int IF = 5;
    public static final int ELSE = 6;
    public static final int FLOAT = 7;
    public static final int INT = 8;
    public static final int CHAR = 9;
    public static final int VOID = 10;
    public static final int RETURN = 11;

    //运算符
    public static final int PLUS = 101;
    public static final int SUB = 102;
    public static final int MUL = 103;
    public static final int DIV = 104;
    public static final int REMAINDER = 105;  //取模%
    public static final int ASSIGN = 106;  // 赋值=
    public static final int BT = 107;  //大于
    public static final int LT = 108;  //小于
    public static final int EQ = 109;  //等于==
    public static final int BET = 110; //大于等于
    public static final int LET = 111; //小于等于
    public static final int NEQ = 112; //不等号 !=
    public static final int PP = 113; //加加 ++
    public static final int MM = 114; //减减 --
    public static final int CAND = 115; //条件与&&
    public static final int COR = 116; //条件或||
    public static final int PLUSE = 117; // +=
    public static final int MINE = 118; // -=
    public static final int MULE = 119; // *=
    public static final int DIVE = 120; // /=
    public static final int REMAINDERE = 121; // %=

    /**
     * 边界符 () {} ; , []
     */
    public static final int SEPARATORS = 201;

    //标识符
    public static final int IDN = 301;

    //整数，浮点数
    public static final int INTNUM = 401;  //整数
    public static final int FLONUM = 402;  //浮点数

    //单字符
    public static final int SINGLECHAR = 501;

    //字符串
    public static final int STR = 601;

    //错误字符
    public static final int ERROR = -1;
}
