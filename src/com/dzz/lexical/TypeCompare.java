package com.dzz.lexical;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.TreeMap;
import java.util.Comparator;
import java.util.HashMap;

public class TypeCompare {
    private static final String keyword[] = {
      "while", "continue", "break", "for", "if", "else", "float", "int", "char", "void", "return"
    };

    private static final String se[] = {
      "(",")","{","}","[","]",",",";"
    };

    private static final String op[] = {
      "+","-","*","/","=","%",">","<",">=","<=","==","!=","++","--","+=","-=","*=","/=","%=","&&","||"
    };
    
    private static final String trans[] = {
      "\\a","\\b","\\f","\\n","\\r","\\t","\\v","\\","\\'","\\\"", "\\\\"
    };
    
    private Map<Integer, String> map;
    private Map<String, String> opMap;
    public TypeCompare() {
    	map = new HashMap<>();
    	opMap = new HashMap<>();
    	setmap(opMap);
    }
    
    public boolean isKeyWord(String str){
        for(int i=0;i<keyword.length;i++){
            if(str.equals(keyword[i])) return true;
        }
        return false;
    }

    public boolean isSe(String str){
        for(int i=0;i<se.length;i++){
            if(str.equals(se[i])) return true;
        }
        return false;
    }

    public boolean isOp(String str){
        for(int i=0;i<op.length;i++){
            if(str.equals(op[i])) return true;
        }
        return false;
    }

    public boolean isFloat(String str){
        String[] temp = str.split("\\.");
        if(temp.length != 2)
        return false;
        return true;
    }
    
    public boolean isTransChar(String token){
        String str = token.substring(1,token.length()-1);
        for(int i=0;i<trans.length;i++){
            if(str.equals(trans[i])) return true;
        }
        return false;
    }

    public boolean isTrans(String token){
        for(int i=0;i<trans.length;i++){
            if(token.equals(trans[i])) return true;
        }
        return false;
    }

    public boolean isTransString(String token){
        String str = token.substring(1,token.length()-1);
        if(str.charAt(str.length()-1)=='\\') return false;
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)=='\\'){
                char ch = str.charAt(i);
                i++;
                String temp = "";
                temp = temp+ch+str.charAt(i);
                if(!isTrans(temp)) return false;
            }
        }
        return true;
    }

    public int getType(String type) {
        Field[] fields = SymbolTable.class.getDeclaredFields();
        for(Field field : fields){
            if(field.getName().equals(type)){
            	try {
            		Integer tmp = (Integer) field.get(new SymbolTable());
                    addMap(tmp, type);
            		return tmp;
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        return -1;
    }
    
    public Map<Integer, String> getSymbolTable(){
    	if (map == null || map.isEmpty()) {
			return null;
		}
		Map<Integer, String> sortedMap = new TreeMap<Integer, String>(new Comparator<Integer>() {
			public int compare(Integer key1, Integer key2) {
				return key1 - key2;
			}});
		sortedMap.putAll(map);
		return sortedMap;
    }
    
    public Map<String, String> getOpMap(){
    	return this.opMap;
    }
    
    public void addMap(Integer key, String value) {
    	if(!map.containsKey(key))
    		map.put(key, value);
    }
    public void setmap(Map<String, String> map) {
    	map.put("+", "PLUS");
    	map.put("-", "SUB");
    	map.put("*", "MUL");
    	map.put("/", "DIV");
    	map.put("%", "REMAINDER");
    	map.put("=", "ASSIGN");
    	map.put(">", "BT");
    	map.put("<", "LT");
    	map.put("==", "EQ");
    	map.put(">=", "BET");
    	map.put("<=", "LET");
    	map.put("!=", "NEQ");
    	map.put("++", "PP");
    	map.put("--", "MM");
    	map.put("&&", "CAND");
    	map.put("||", "COR");
    	map.put("+=", "PLUSE");
    	map.put("-=", "MINE");
    	map.put("*=", "MULE");
    	map.put("/=", "DIVE");
    	map.put("%=", "REMAINDERE");
    }
}
